
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include "xxhash.c"

// struct for a bloom filter
struct bloom_filter
{
	double false_positive_rate;
	int capacity;
	int word_size;
	int bit_size;
	int hash_functions;
	int elements;
	long *filter;
};


// Initialize a bloom filter
bool blf_alloc(struct bloom_filter *bf)
{
	double fpr;
	int cap;
	int bits;
	int words;
	int hf;
	long *filt;

	fpr = bf->false_positive_rate;
	cap = bf->capacity;

	bits = (int) ceil(cap * log(1/fpr) / pow(log(2),2));
	hf = (int) round((float)bits * log(2) / cap);
	words = (int) ceil((float)bits / sizeof(long));

	filt = (long *) malloc(sizeof(long)*words);
	if (filt == NULL)
	{
		return false;
	}
	memset(filt, 0, sizeof(long)*words);

	bf->elements = 0;
	bf->word_size = words;
	bf->bit_size = (words << 3) * sizeof(long);
	bf->hash_functions = hf;
	bf->filter = filt;

	return true;
}


// Destroy a bloom filter
void blf_free(struct bloom_filter *bf)
{
	free(bf->filter);
}


// Add a string to the bloom filter
bool blf_add(struct bloom_filter *bf, const char *element, int len)
{
	int seed;
	int bit_index;
	int word_index;
	int bit_in_word;
	long *filt;
	filt = bf->filter;

	if(bf->elements == bf->capacity)
	{
		return false;
	}

	for(seed = 0; seed < bf->hash_functions; seed++)
	{
		bit_index = XXH32(element, len, seed) % bf->bit_size;
		word_index = (bit_index >> 3) / sizeof(long);
		bit_in_word = bit_index % (sizeof(long) << 3);
		filt[word_index] |= (long)0x1 << bit_in_word;
	}
	bf->elements++;
	return true;
}


// Check the bloom filter for a string
bool blf_check(struct bloom_filter *bf, const char *element, int len)
{
	int seed;
	int bit_index;
	int word_index;
	int bit_in_word;
	int hash_present;
	long *filt;
	filt = bf->filter;

	for(seed = 0; seed < bf->hash_functions; seed++)
	{
		bit_index = XXH32(element, len, seed) % bf->bit_size;
		word_index = (bit_index >> 3) / sizeof(long);
		bit_in_word = bit_index % (sizeof(long) << 3);
		hash_present = (bool) (filt[word_index] & ((long)0x1 << bit_in_word));
		if (!hash_present)
		{
			return false;
		}
	}
	return true;
}


// Print the content of the bloom filter
void blf_print(struct bloom_filter *bf, bool full)
{
	int i;
	int j;

	printf("Capacity %d\n", bf->capacity);
	printf("False positive rate %f\n", bf->false_positive_rate);
	printf("Hash funcions %d\n", bf->hash_functions);
	printf("Word size %d\n", bf->word_size);
	printf("Bit size %d\n", bf->bit_size);
	printf("Elements added %d\n", bf->elements);

	if(full)
	{
		for(i=0; i<bf->word_size; i++)
		{
			for(j=0; j < (sizeof(long) << 3); j++)
			{
				if(j == (sizeof(long) << 2)) printf("|");
				int bit = (bool)(bf->filter[i] & ((long)0x1 << j));
				printf("%1d", bit);
			}
			printf(" word%d\n", i);
		}	
	}
}


// Test the performance of the bloom filter
void blf_test(int capacity, double fpr, const char *seed, int seed_len)
{
	char *test = (char *)malloc((seed_len + capacity/10 + 1) * sizeof(char));
	int i;
	int actual_false_positives = 0;
	double actual_fpr;

	struct bloom_filter bf;
	bf.capacity = capacity;
	bf.false_positive_rate = fpr;

	blf_alloc(&bf);

	// Add a bunch of strings to it
	for(i = 0; i < capacity; i++)
	{
		sprintf(test, "%s%dA", seed, i);
		blf_add(&bf, test, (seed_len + capacity/10 + 1));
	}

	// Check for stuff that's not in there
	for(i = 0; i < capacity; i++)
	{
		sprintf(test, "%s%dB", seed, i);
		if (blf_check(&bf, test, (seed_len + capacity/10 + 1)))
		{
			actual_false_positives++;
		}
	}

	actual_fpr = (double)actual_false_positives / (double)capacity;

	printf("False positives %d\n", actual_false_positives);
	printf("Tested false positive rate %f\n", actual_fpr);

	free(test);

	blf_free(&bf);
}


