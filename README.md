# Python Bloom Filter #

## Setup ##
* Clone the repository
* Install dependencies ([xxhash](https://code.google.com/p/xxhash/))
* Copy bloomfilter.h to your project folder

## Usage ##
The following code example illustrates how to use this Bloom filter class in c.

```
#!c

#include "bloomfilter.h"

int main()
{
	// A new bloom filter
	struct bloom_filter bf;
	bf.capacity = 1000000;
	bf.false_positive_rate = 0.0001;
	blf_alloc(&bf);
	// After setting the capacity and desired false positive rate,
	// call blf_alloc and pass it a pointer to the new struct.
	// This will determine the bit size and number of hash
	// functions required for the correct false positive rate and
	// allocate memory to store the filter. If this function returns
	// false it means that memory allocation has failed.

	// Adding an item to the bloom filter, returns false if
	// the add failed (the bloom filter is full). When adding/checking
	// the 3rd argument is the length in chars.
	blf_add(&bf, "Hello world!", 12);
	// true

	// True means the item is probably in the bloom filter
	blf_check(&bf, "Hello world!", 12);
	// true

	// False means the item is definitely not in the bloom filter
	blf_check(&bf, "Bounjour le monde!", 18);
	// false

	// Don't forget to free the memory allocated for the filter
	blf_free(&bf);

	// There are some other things you can do, see the documentation
	// for more information
	return 0;
}
```